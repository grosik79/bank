import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {
    private BankAccount account = new BankAccount();
    private ExecutorService service = Executors.newFixedThreadPool(5);

    public void subRequest(double howMuch){
service.submit(new Request(account, TransactionType.Sub, howMuch));
    }
    public void addRequest(double howMuch){
        service.submit(new Request(account, TransactionType.Add, howMuch));

    }
    public void balance(){
        account.balance();
    }

}
